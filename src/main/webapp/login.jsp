<%@page import="edu.uclm.esi.apalabreitorNoSpring.dao.UserDAO"%>
<%@page import="edu.uclm.esi.apalabreitorNoSpring.model.User"%>
<%@ page language="java" pageEncoding="UTF-8"%>

<%
	String userName = request.getParameter("userName");
	String pwd = request.getParameter("pwd");
	
	try {
		User user = UserDAO.identify(userName, pwd);
		session.setAttribute("user", user);
		response.setStatus(200);
	}
	catch(Exception e) {
		response.setStatus(404);
	}
	response.flushBuffer();
%>