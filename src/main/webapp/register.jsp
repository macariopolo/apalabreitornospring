<%@page import="edu.uclm.esi.apalabreitorNoSpring.dao.UserDAO"%>
<%@page import="edu.uclm.esi.apalabreitorNoSpring.model.User"%>
<%@ page language="java" pageEncoding="UTF-8"%>

<%
	String email = request.getParameter("email");
	String userName = request.getParameter("userName");
	String pwd1 = request.getParameter("pwd1");
	String pwd2 = request.getParameter("pwd2");
	
	if (pwd1==null || pwd2==null)
		response.sendError(400, "Empty passwords");
	else if (!pwd1.equals(pwd2))
		response.sendError(400, "Passwords do not match");
	else {
		try {
			User.insert(email, userName, pwd1);
		}
		catch (Exception e) {
			response.sendError(409, e.getMessage());
		}
	}
%>