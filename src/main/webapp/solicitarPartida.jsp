<%@page import="org.json.JSONObject"%>
<%@page import="edu.uclm.esi.apalabreitorNoSpring.model.*, edu.uclm.esi.apalabreitorNoSpring.web.controllers.*"%>
<%@page import="edu.uclm.esi.apalabreitorNoSpring.dao.UserDAO"%>
<%@page import="edu.uclm.esi.apalabreitorNoSpring.model.User"%>
<%@ page language="java" contentType="application/text" pageEncoding="UTF-8"%>

<%
	String action = request.getParameter("action");
	JSONObject jso=new JSONObject();
	if (session.getAttribute("user")==null) {
		response.getWriter().print(jso.toString());
		response.sendError(403, "Identifícate antes de jugar");
	} else {
		User user = (User) session.getAttribute("user");
		if (action.equals("Nueva partida")) {
			Match match = new Match();
			match.setPlayerA(user);
			Manager.get().pendingMatches.add(match);
			jso.put("type", "PARTIDA CREADA");
			jso.put("idPartida", match.getId());
			response.getWriter().print(jso.toString());
		} else if (action.equals("Unir a partida")) {
			if (Manager.get().pendingMatches.isEmpty()) {
				response.getWriter().print(jso.toString());
				response.sendError(404, "Identifícate antes de jugar");
			} else {
				Match match = Manager.get().pendingMatches.remove(0);
				match.setPlayerB(user);
				Manager.get().inPlayMatches.put(match.getId(), match);
				jso.put("type", "PARTIDA LISTA");
				jso.put("idPartida", match.getId());
				response.getWriter().print(jso.toString());
			}
		}
	}
%>