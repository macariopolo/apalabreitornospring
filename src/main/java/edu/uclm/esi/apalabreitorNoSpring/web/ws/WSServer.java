package edu.uclm.esi.apalabreitorNoSpring.web.ws;

import java.util.concurrent.ConcurrentHashMap;

import javax.servlet.http.HttpSession;
import javax.websocket.EndpointConfig;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.ServerEndpoint;

import org.json.JSONObject;

import edu.uclm.esi.apalabreitorNoSpring.model.Match;
import edu.uclm.esi.apalabreitorNoSpring.model.User;
import edu.uclm.esi.apalabreitorNoSpring.web.controllers.Manager;

@ServerEndpoint(value="/wsServer", configurator=HttpSessionConfigurator.class)
public class WSServer {
	private static ConcurrentHashMap<String, Session> sessionsByUser=new ConcurrentHashMap<>();
	
	@OnOpen
	public void open(Session sesion, EndpointConfig config) {
		HttpSession httpSession=(HttpSession) config.getUserProperties().get(HttpSession.class.getName());
		User user = (User) httpSession.getAttribute("user");
		user.setWebSocketSession(sesion);
		sessionsByUser.put(user.getUserName(), sesion);
	}
	
	@OnMessage
	public void recibir(Session session, String msg) throws Exception {
		System.out.println(msg);
		JSONObject jso=new JSONObject(msg);
		String type = jso.getString("type");
		switch (type) {
		case "INICIAR PARTIDA" :
			String idPartida = jso.getString("idPartida");
			Match match = Manager.get().inPlayMatches.get(idPartida);
			Manager.get().indexMatch(match);
			match.start();
			break;
		case "MOVIMIENTO" :
			idPartida = jso.getString("idPartida");
			match = Manager.get().inPlayMatches.get(idPartida);
			match.playerPlays(session.getId(), jso.getJSONArray("casillas"));
			break;
		case "CAMBIO DE LETRAS" :
			break;
		case "PASO DE TURNO" :
			idPartida = jso.getString("idPartida");
			match = Manager.get().inPlayMatches.get(idPartida);
			match.pasarTurno();
			break;
		case "ABANDONO" :
			break;
		}
	}

	private void sendError(Session session, String message) throws Exception {
		JSONObject jso = new JSONObject();
		jso.put("TYPE", "ERROR");
		jso.put("MESSAGE", message);
		session.getBasicRemote().sendText(jso.toString());
	}
}
