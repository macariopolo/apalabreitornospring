package edu.uclm.esi.apalabreitorNoSpring.web.controllers;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

import edu.uclm.esi.apalabreitorNoSpring.model.Match;
import edu.uclm.esi.apalabreitorNoSpring.model.User;

public class Manager {
	private List<User> users = new ArrayList<>();
	public List<Match> pendingMatches = new ArrayList<>();
	public ConcurrentMap<String, Match> inPlayMatches = new ConcurrentHashMap<>();
	private ConcurrentMap<String, Match> matchesBySession = new ConcurrentHashMap<>();

	private Manager() {
	}
	
	private static class ManagerHolder {
		static Manager singleton=new Manager();
	}
	
	public static Manager get() {
		return ManagerHolder.singleton;
	}

	public void indexMatch(Match match) {
		matchesBySession.put(match.getPlayerA().getSession().getId(), match);
		matchesBySession.put(match.getPlayerB().getSession().getId(), match);
	}


}
