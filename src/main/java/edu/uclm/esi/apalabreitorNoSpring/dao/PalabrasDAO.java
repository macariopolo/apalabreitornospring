package edu.uclm.esi.apalabreitorNoSpring.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import edu.uclm.esi.apalabreitorNoSpring.model.*;

public class PalabrasDAO {

	public static List<Palabra> find(String texto) throws Exception {
		Connection bd=null;
		try {
			bd=Broker.get().getBd();
			String sql="select texto from palabra where texto=?";
			PreparedStatement ps=bd.prepareStatement(sql);
			ps.setString(1, texto);
			ResultSet rs=ps.executeQuery();
			ArrayList<Palabra> result=new ArrayList<>();
			while (rs.next()) {
				Palabra palabra = new Palabra(texto);
				result.add(palabra);
			}
			return result;
		}
		catch (Exception e) {
			throw e;
		}
		finally {
			if (bd!=null)
				bd.close();			
		}
	}
}
