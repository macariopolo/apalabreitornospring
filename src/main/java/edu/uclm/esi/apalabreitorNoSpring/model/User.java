package edu.uclm.esi.apalabreitorNoSpring.model;

import java.io.IOException;

import javax.websocket.Session;

import org.json.JSONObject;
import edu.uclm.esi.apalabreitorNoSpring.dao.UserDAO;

public class User {
	private String userName;
	private String email;
	private Session session;
	
	public User() {
	}
		
	public String getUserName() {
		return userName;
	}
	
	public void setUserName(String userName) {
		this.userName = userName;
	}
	
	public String getEmail() {
		return email;
	}
	
	public void setEmail(String email) {
		this.email = email;
	}

	public static User insert(String email, String userName, String pwd) throws Exception {
		User player=new User();
		player.setEmail(email);
		player.setUserName(userName);
		UserDAO.insert(player, pwd);
		return player;
	}

	public static User identify(String userName, String pwd) throws Exception {
		return UserDAO.identify(userName, pwd);
	}
	
	public void sendMessage(ResultadoJugada resultado) throws IOException {
		JSONObject jso = resultado.toJSON();
		this.sendMessage(jso.toString());
	}

	public void sendMessage(String message) throws IOException {
		session.getBasicRemote().sendText(message);
	}
	
	public void setWebSocketSession(Session session) {
		this.session = session;
	}

	public Session getSession() {
		return this.session;
	}
}
