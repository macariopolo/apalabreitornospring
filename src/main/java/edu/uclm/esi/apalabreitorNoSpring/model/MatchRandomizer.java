package edu.uclm.esi.apalabreitorNoSpring.model;

import java.security.SecureRandom;

public class MatchRandomizer {
	private boolean randomize;
	private SecureRandom dado;
	private Board board;

	public MatchRandomizer(boolean randomize) {
		this.randomize = randomize;
		if (randomize)
			this.dado=new SecureRandom();
	}

	public User randomizePlayer(User... players) {
		if (randomize) {
			int n = dado.nextInt(players.length);
			return players[n];
		}
		return players[0];
	}

	public void disorderLetters(Board board) {
		this.board = board;
		if (randomize) {
			addLetter(12, 'A'); addLetter(2, 'B'); addLetter(4, 'C'); addLetter(5, 'D');
			addLetter(12, 'E'); addLetter(1, 'F'); addLetter(2, 'G'); addLetter(2, 'H');
			addLetter(6, 'I');  addLetter(1, 'J');                    addLetter(4, 'L');
			addLetter(2, 'M');  addLetter(5, 'N'); addLetter(1, 'Ñ'); addLetter(9, 'O');
			addLetter(2, 'P');  addLetter(1, 'Q'); addLetter(5, 'R'); addLetter(6, 'S');
			addLetter(4, 'T');  addLetter(5, 'U'); addLetter(1, 'V'); 
			addLetter(1, 'X');  addLetter(1, 'Y'); addLetter(1, 'Z');		
			
			for (int i=0; i<300; i++) {
				int posA = dado.nextInt(board.letters.size());
				int posB = dado.nextInt(board.letters.size());
				Letter letraPosA = board.letters.get(posA);
				board.letters.set(posA, board.letters.get(posB));
				board.letters.set(posB, letraPosA);
			}
		} else {
			addLetters("COLOCARE"); addLetters("DEJARES");
			addLetters("BOJOCAS"); addLetters("ARIMOSP");
		}
	}

	private void addLetters(String cadena) {
		for (int i=0; i<cadena.length(); i++)
			board.letters.add(1, cadena.charAt(i), Board.puntos.get(cadena.charAt(i)));
	}

	private void addLetter(int amount, char c) {
		board.letters.add(amount, c, Board.puntos.get(c));
	}
}
