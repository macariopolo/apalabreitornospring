package edu.uclm.esi.apalabreitorNoSpring.model;

import java.io.IOException;
import java.util.ArrayList;
import java.util.UUID;

import javax.websocket.Session;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class Match {
	private String id;
	private User playerA;
	private User playerB;
	private User jugadorConElTurno;
	private Board board;
	private MatchRandomizer randomizer;
	
	public Match() {
		this.id = UUID.randomUUID().toString();
		this.randomizer = new MatchRandomizer(false);
	}

	public void setPlayerA(User user) {
		this.playerA = user;
	}

	public void setPlayerB(User playerB) {
		this.playerB = playerB;
	}
	
	public String getId() {
		return id;
	}

	public void start() {
		this.jugadorConElTurno = this.randomizer.randomizePlayer(this.playerA, this.playerB);
		this.board = new Board(this.randomizer);
		
		JSONObject jso = new JSONObject();
		try {
			jso.put("type", "START");
			jso.put("letras", this.board.getLetters(7));
			jso.put("turno", jugadorConElTurno==playerA);
			jso.put("oponente", playerB.getUserName());
			this.playerA.sendMessage(jso.toString());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JSONException e) {
			
		}
		try {
			jso.put("letras", this.board.getLetters(7));
			jso.put("turno", jugadorConElTurno==playerB);
			jso.put("oponente", playerA.getUserName());
			this.playerB.sendMessage(jso.toString());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JSONException e) {
			
		}
	}

	public void playerPlays(String idSession, JSONArray jsaJugada) throws Exception {
		ResultadoJugada resultado;
		User player = this.playerA.getSession().getId().equals(idSession) ? playerA : playerB;
		if (player!=this.jugadorConElTurno) {
			resultado = new ResultadoJugada();
			resultado.addException("No tienes el turno");
			player.sendMessage(resultado);
		} else {
			ArrayList<JSONObject> jugada = new ArrayList<>();
			for (int i=0; i<jsaJugada.length(); i++)
				jugada.add(jsaJugada.getJSONObject(i));
			resultado = this.board.movement(jugada);
			
			if (resultado.getExceptions().isEmpty() && resultado.invalid().isEmpty()) {	
				resultado.setTurno(false);
				player.sendMessage(resultado);

				User contrincante = this.playerA==player ? playerB : playerA;
				resultado.ocultarLetras();
				resultado.setTurno(true);
				contrincante.sendMessage(resultado);
				cambiarTurno();
			} else {
				resultado.setTurno(true);
				player.sendMessage(resultado);
				this.board.deshacer(jugada);
			}
		}
	}
	
	public User getPlayerA() {
		return playerA;
	}
	
	public User getPlayerB() {
		return playerB;
	}

	private void cambiarTurno() {
		this.jugadorConElTurno = (this.playerA==this.jugadorConElTurno ? this.playerB : this.playerA);
	}

	public void userLoses(Session session) {
		User loser = this.playerA.getSession()==session ? playerA : playerB;
		User winner = this.playerA.getSession()==session ? playerB : playerA;
		if (loser!=null && winner!=null) {
			JSONObject jso = new JSONObject();
			try {
				jso.put("type", "VICTORIA");
				jso.put("oponente", playerB.getUserName());
				winner.sendMessage(jso.toString());
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (JSONException e) {
				
			}
		}
	}

	public void pasarTurno() {
		cambiarTurno();
		ResultadoJugada resultado=new ResultadoJugada();
		resultado.setTurno(true);
		try {
			this.jugadorConElTurno.sendMessage(resultado);
			resultado.setTurno(false);
			User otro = (this.playerA==this.jugadorConElTurno ? this.playerB : this.playerA);
			otro.sendMessage(resultado);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
